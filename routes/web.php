<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\CartController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/category', function(){
    return view('front.shop');
})->name('category');

Route::get('/product_detail', function(){return view('front.product_detail');})
    ->name('product_detail');

Route::get('/', [FrontController::class, 'index']);
Route::get('/product', [ProductController::class, 'index']);

Route::post('/', [ProductController::class, 'list'])->name('product_detail');


// Backend
Route::get('/home', function(){
    return view('admin.dashboard.index');
})->middleware(['auth','verified']);
//Product
Route::prefix('product')->middleware(['auth','verified'])->group(function () {
    Route::get('/',[ProductController::class, 'index'])->name('product');
    Route::get('/create',[ProductController::class, 'create'])->name('product.create');
    Route::post('/store',[ProductController::class, 'store'])->name('product.store');
    Route::get('/edit/{id}',[ProductController::class, 'edit'])->name('product.edit');
    Route::post('/update/{id}',[ProductController::class, 'update'])->name('product.update');
    Route::get('/delete/{id}',[ProductController::class, 'delete'])->name('product.delete');
    Route::get('/search',[ProductController::class, 'search'])->name('product.search');
});
//Category
Route::prefix('category')->middleware(['auth','verified'])->group(function () {
    Route::get('/',[CategoryController::class, 'index'])->name('category');
    Route::get('/create',[CategoryController::class, 'create'])->name('category.create');
    Route::post('/store',[CategoryController::class, 'store'])->name('category.store');
    Route::get('/edit/{id}',[CategoryController::class, 'edit'])->name('category.edit');
    Route::post('/update/{id}',[CategoryController::class, 'update'])->name('category.update');
    Route::get('/delete/{id}',[CategoryController::class, 'delete'])->name('category.delete');
    Route::get('/search',[CategoryController::class, 'search'])->name('category.search');
});
//cart
Route::prefix('cart')->middleware(['auth','verified'])->group(function (){
    Route::get('/', [CartController::class, 'index'])->name('cart');
    Route::post('/store', [CartController::class, 'store'])->name('cart.store');
});
