<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class FrontController extends Controller
{
    public function product()
    {
        $products = Product::where('status', 1)->get();
        return view('front.index', compact('products'));
    }
}
