<?php

namespace App\Http\Controllers;   

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function list() {
        $products = Product::paginate(5);
        return view('front.index', compact('products'));  
    }
    public function index() {
        $products = Product::paginate(5);
        return view('admin.product.index', compact('products'));
    }
    public function create() {
        return view('admin.product.create');
    }
    public function store(Request $request){
        $product = Product::create([
            'name' => $request->name,
            'detail' => $request->detail,
            'qty' => $request->qty,
            'price' => $request->price,
            'status' => $request->status,
        ]);
    }
    public function edit($id){
        $data = Product::find($id);
        return view('admin.product.edit', compact('data'));
    }
    public function update(Request $request, $id){
        $product = Product::where('id', $request->id)->update([
            'name' => $request->name,
            'detail' => $request->detail,
            'qty' => $request->qty,
            'price' => $request->price,
            'status' => $request->status,
        ]);
    }
    public function delete($id){
        $product = Product::find($id);
        $product->delete();
        return redirect()->route('product.index');
    }
    public function search(Request $request){
        $products = Product::where('name', 'LIKE', '%'.$request->search.'%')->paginate(5);
        return view('admin.product.index', compact('products'));
    }
}