<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::paginate(5);
        return view('admin.category.index', compact('categories'));
    }
    public function create(){
        return view('admin.category.create');
    }
    public function store(Request $request){
        $categories = Category::create([
            'name' => $request->name,
            'sequence' => $request->sequence,
            'enable_status' => $request->enable_status,
        ]);
    }
    public function edit($id){
        $data = Category::find($id);
        return view('admin.category.edit', compact('data'));
    }
    public function update(Request $request, $id){
        $category = Category::where('id', $request->id)->update([
            'name' => $request->name,
            'sequence' => $request->sequence,
            'enable_status' => $request->enable_status,
        ]);
    }
    public function delete($id){
        $category = Category::find($id);
        $category -> delete();
        return redirect()->route('category');
    }
    public function search(Request $request){
        $categories = Category::where('name', 'LIKE', '%'.$request->search.'%')->paginate(5);
        return view('admin.category.index', compact('categories'));
    }
}
