const { default: Axios } = require("axios");

new Vue({
    el: "#cart",
    data:{
        data:{
            qty: ''
        }
    },
    methods: {
        create(){
            axios.post('/cart/store/', this.data)
            .then(res => {
                console.log(res);
                window.location.href="/product_detail";
            });
        }
    },
});