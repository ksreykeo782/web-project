const { default: Axios } = require("axios");

new Vue({
    el: "#create",
    data:{
        data:{
            name: '',
            detail: '',
            qty: '',
            price: '',
            status: true,
        }
    },
    methods: {
        create(){
            axios.post('/product/store/', this.data)
            .then(window.location.href="/product");
        }
    },
});