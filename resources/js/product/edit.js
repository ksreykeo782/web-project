const { default: Axios } = require("axios");

new Vue({
    el: "#editProduct",
    data:{
        data:data
    },
    methods: {
        editProduct(){
            axios.post('/product/update/' + this.data.id, this.data)
            .then(window.location.href="/product");
        }
    },
});