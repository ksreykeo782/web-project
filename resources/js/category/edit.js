const { default: Axios } = require("axios");

new Vue({
    el: "#editCategory",
    data: {
        data: data
    },
    methods: {
        editCategory(){
            axios.post('/category/update/' + this.data.id, this.data)
            .then(window.location.href = "/category");
        }
    },
});