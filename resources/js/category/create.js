const { default: Axios } = require("axios");

new Vue({
   el: "#addCategory",
   data: {
        data:{
           name: '',
           sequence: 1,
           enable_status: true
       },
   },
   methods: {
        addCategory(){
            axios.post('/category/store/', this.data)
            .then(window.location.href="/category");
        }
   },
});