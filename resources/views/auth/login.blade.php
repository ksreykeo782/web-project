<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>login</title>

  <!-- Favicon Icon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/logo/2328435-200.png') }}">
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{asset('web/css/login.css')}}">
</head>
<body style="background-color: #d0d0ce">
  <div class="container h-100">
    <div class="row align-items-center min-vh-100 ">
        <div class="col-12 mx-auto">
            <div class="card shadow-lg p-3 card-rounded-shape">              
              <div class="row align-items-center">
                <div class="col-md-1"></div>
                <div class="col-md-4 col-sm-6">
                  <div class="header">
                      <img src="{{ asset('img/logo/logo.png') }}" width="150px" style="margin-left: -9px">
                    <h4 class="mb-4 sub-header">Sign into your account</h4>
                  </div>
                  <form action="/login" method="POST">
                    @csrf    
                    @php $error = session()->get('error'); @endphp    
                   
                    <div class="form-group">
                      <label for="email" class="d-none d-md-block">Email</label>
                      <input type="email" name="email" id="email" class="form-control" placeholder="Email address">
                      @error('email')
                      <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>

                    <div class="form-group mb-4">
                      <label for="password" class="d-none d-md-block">Password</label>
                      <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                      @error('password')
                      <span class="text-danger">{{ $message }}</span>
                      @enderror
                    </div>

                    <button class="button-submit" type="submit">Log In</button>
                  </form>

                  <span>
                    <a class="register-here" href="{{route('password.request') }}">Forgot password?</a>
                  </span><br>

                  <span class="login-card-footer-text">Account exist? <a href="/register" class="register-here">Register here</a></span>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6 col-sm-6 d-none d-md-block">
                  <img src="{{ asset('img/logo/man.jpg') }}" width="100%">
                </div>
              </div>
            </div>
        </div>
    </div>
  </div>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>




