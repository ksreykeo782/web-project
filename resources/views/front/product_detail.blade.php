@extends('front.layout')
@section('content')
     @if(session('message'))
        {{session('message')}}
     @endif
        <!-- ================ start banner area ================= -->	
        <section class="blog-banner-area" id="blog">
            <div class="container h-100">
                <div class="blog-banner">
                    <div class="text-center">
                        <h1>Shop Single</h1>
                        <nav aria-label="breadcrumb" class="banner-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Shop Single</li>
                </ol>
              </nav>
                    </div>
                </div>
            </div>
        </section>
        <div class="product_image_area" id="cart">
            <div class="container">
                <div class="row s_product_inner">
                    <div class="col-lg-6">
                        <div class="owl-carousel owl-theme s_Product_carousel">
                            <div class="single-prd-item">
                                <img class="img-fluid" src="web/img/product/product1.png" alt="">
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-5 offset-lg-1">
                            <div class="s_product_text">
                                <h3>Faded SkyBlu Denim Jeans</h3>
                                <h2>$149.99</h2>
                                <ul class="list">
                                    <li><a class="active" href="#"><span>Size</span> : Household</a></li>
                                </ul>
                                <p>Mill Oil is an innovative oil filled radiator with the most modern technology. If you are looking for
                                    something that can make your interior look awesome, and at the same time give you the pleasant warm feeling
                                    during the winter.
                                </p>
                                <form method="POST">
                                    @csrf

                                    <div class="product_count">
                                        <label for="qty">Quantity:</label>
                                        <input type="number" name="quantity" class="input-text qty" v-model="data.qty">
                                    </div>
                                    <button type="button" @click="create()" class="button primary-btn" >Add to Cart</button>               
        
                                </form>
                                
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <script src="{{ mix('js/app.js')}}"></script>
        <script src="{{ mix('js/product_detail/product_detail.js')}}"></script>
@endsection