@extends('admin.layout.master')
@section('name_header')
    <h5>DASHBOARD</h5>
@endsection
@section('right_header')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="">Home</a></li>
    <li class="breadcrumb-item active">Dashboard</li>
</ol>
@endsection
@section('contents')
<section class="content">
    <div class="row" style="margin: auto">
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>PRODUCTS</h3>
              <p>App Products</p>
            </div>
            <div class="icon">
              <i class="fas fa-bars"></i>
            </div>
            <a href="{{ route('product')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-6 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>CATEGORIES</h3>
              <p>App Categories</p>
            </div>
            <div class="icon">
              <i class="fas fa-file-image"></i>
            </div>
            <a href="{{ route('category')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
</section>
@endsection