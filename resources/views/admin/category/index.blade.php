@extends('admin.layout.master')
@section('name_header')
    <h5>CATEGORY LIST</h5>
@endsection
@section('right_header')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{ route('category')}}">Home</a></li>
    <li class="breadcrumb-item active">Categories</li>
</ol>
@endsection
@section('title_content')
<div class="d-flex justify-content-between">
    <a href="{{route('category.create')}}"><button class="btn btn-primary">+ Add New</button></a>
    <form class="form-inline " action="{{ route('category.search') }}" method="GET" style="margin-left: 640px">
        @csrf
        <input type="text" class="form-control mr-sm-2" placeholder="Search by Category Name" aria-label="Search" name="search">
        <button class="btn btn-primary my-2 my-sm-0" type="submit">Search</button>
    </form>
</div>
@endsection
@section('contents')
    @include('admin.category.table')
    {{-- {{$categories->links()}} --}}
@endsection