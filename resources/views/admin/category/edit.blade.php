@extends('admin.layout.master')
@section('right_header')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{route('category')}}">BACK</a></li>
    <li class="breadcrumb-item active">Edit Category</li>
</ol>
@endsection
@section('title_content')
    <h3>Edit Category</h3> 
@endsection
@section('contents')
    <form id="editCategory" v-cloak>
        @csrf
        <div class="card-body">
            @include('admin.category.form')
            <button type="button" @click="editCategory()" class="btn btn-primary">Update</button>
        </div>
    </form>
@endsection
@section('script')
    <script>
        const data = @json($data)
    </script>
    <script src="{{ mix('js/app.js')}}"></script>
    <script src="{{ mix('js/category/edit.js')}}"></script>
@endsection