<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" v-cloak>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th class="text-center">Sequence</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
            @foreach ($categories as $category)
                <tbody>
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$category->name}}</td>
                        <td class="text-center">{{$category->sequence}}</td>
                        <td class="text-center">{{$category->enable_status ? 'Active' : 'Deactive'}}</td>
                        <td class="text-center">
                            <a href="{{route('category.edit', $category->id)}}"><button class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                            <a href="{{route('category.delete', $category->id)}}"><button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></a>
                        </td>
                    </tr>
                </tbody>
            @endforeach
            </table>
        </div>
    </div>
</div>