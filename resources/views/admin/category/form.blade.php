<div class="form-group">
    <label for="name">Name:</label>
    <input type="text" class="form-control" placeholder="Enter name" v-model = "data.name">
</div>
<div class="form-group">
    <label for="order">Sequence:</label>
    <input type="number" class="form-control" placeholder="Choose Sequence" v-model = "data.sequence">
</div>
<div class="form-group">
    <input type="checkbox" name="status" v-model="data.enable_status">
    <label for="status">Status</label>
</div>