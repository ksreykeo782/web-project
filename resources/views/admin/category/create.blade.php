@extends('admin.layout.master')
@section('right_header')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{route('category')}}">BACK</a></li>
    <li class="breadcrumb-item active">Add Category</li>
</ol>
@endsection
@section('title_content')
    <h3>Add Category</h3> 
@endsection
@section('contents')
    <form id="addCategory" v-cloak>
        <div class="card-body">
            @include('admin.category.form')
            <button type="button" @click="addCategory()" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
@section('script')
    <script src="{{ mix('js/app.js')}}"></script>
    <script src="{{ mix('js/category/create.js')}}"></script>
@endsection