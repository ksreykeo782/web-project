<div class="container">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" v-cloak>
                <tr>
                    <thead>
                        <th>#</th>
                        <th>Name</th>
                        <th>Detail</th>
                        <th class="text-center">Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </thead>
                </tr>
            @foreach ($products as $product)
                <tr>
                    <tbody>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->detail}}</td>
                        <td class="text-center">{{$product->qty}}</td>
                        <td class="text-center">{{$product->price}}</td>
                        <td class="text-center">{{$product->status ? 'Active' : 'Deactive'}}</td>
                        <td class="text-center">
                            <a href="{{route('product.edit', $product->id)}}"><button class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                            <a href="{{route('product.delete', $product->id)}}"><button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button></a>
                        </td>
                    </tbody>
                </tr>
            @endforeach
            </table>
        </div>
    </div>
</div>