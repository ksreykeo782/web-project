@extends('admin.layout.master')
@section('right_header')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{ route('product')}}">BACK</a></li>
    <li class="breadcrumb-item active">Add Product</li>
</ol>
@endsection
@section('title_content')
    <h3>Add Product</h3> 
@endsection
@section('contents')
    <form id="create" v-cloak>
        <div class="card-body">
            @include('admin.product.form')
            <button type="button" @click="create()" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection
@section('script')
    <script src="{{ mix('js/app.js')}}"></script>
    <script src="{{ mix('js/product/create.js')}}"></script>
@endsection