<div class="form-group">
    <label for="name">Name:</label>
    <input type="text" class="form-control" placeholder="Enter name" v-model = "data.name" >
</div>
<div class="form-group">
    <label for="detail">Detail:</label>
    <input type="text" class="form-control" placeholder="Enter some detail..."  v-model = "data.detail">
</div>
<div class="form-group">
    <label for="qty">Quantity:</label>
    <input type="number" class="form-control" placeholder="Enter Quantity"  v-model = "data.qty">
</div>
<div class="form-group">
    <label for="price">Price:</label>
    <input type="number" class="form-control" placeholder="Enter Price"  v-model = "data.price">
</div>
<input type="checkbox" name="status" v-model="data.status"> Status