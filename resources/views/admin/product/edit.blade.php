@extends('admin.layout.master')
@section('right_header')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="{{ route('product')}}">BACK</a></li>
    <li class="breadcrumb-item active">Edit Product</li>
</ol>
@endsection
@section('title_content')
    <h3>Edit Product</h3> 
@endsection
@section('contents')
    <form id="editProduct" v-cloak>
        @csrf
        <div class="card-body">
            @include('admin.product.form')
            <button type="button" @click="editProduct()" class="btn btn-primary">Update</button>
        </div>
    </form>
@endsection
@section('script')
    <script>
       const data = @json($data);
    </script>
    <script src="{{ mix('js/app.js')}}"></script>
    <script src="{{ mix('js/product/edit.js')}}"></script>
@endsection