const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/js').vue()
.js('resources/js/product/create.js', 'public/js/product')
.js('resources/js/product/edit.js', 'public/js/product')
.js('resources/js/category/create.js', 'public/js/category')
.js('resources/js/category/edit.js', 'public/js/category')
.js('resources/js/product_detail/product_detail.js', 'public/js/product_detail')
 .postCss('resources/css/app.css', 'public/css', [
     //
 ])
